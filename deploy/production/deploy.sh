#!/usr/bin/env bash
# Blue/Green Deployment script for staging
set -e

rsync -az --delete --delete-excluded --exclude-from=deploy/production/.rsyncignore ./ ${PRODUCTION_SSH_USER}@${PRODUCTION_SSH_HOST}:${PRODUCTION_BASE_DIR}/cache/

TIMESTAMP=$(date +%s)
PHP_BIN=/usr/bin/php72

ssh -T ${PRODUCTION_SSH_USER}@${PRODUCTION_SSH_HOST} <<_EOF_
    # pre-prepare
    set -xe
    cd ${PRODUCTION_BASE_DIR}

    # prepare
    mkdir -p releases/${TIMESTAMP}/
    mkdir -p logs
    mkdir -p shared/data/fileadmin/
    mkdir -p backups
    rsync -a cache/ releases/${TIMESTAMP}/

    # post-prepare
    ln -s ../../../shared/data/fileadmin/ releases/${TIMESTAMP}/public/fileadmin
    ln -s ../../../../../logs/ releases/${TIMESTAMP}/public/typo3temp/var/logs
    cp shared/AdditionalConfiguration.php releases/${TIMESTAMP}/public/typo3conf/AdditionalConfiguration.php

    # Backup
    if [ -L ${PRODUCTION_BASE_DIR}/releases/current/vendor/bin/typo3cms ]; then
        $PHP_BIN ${PRODUCTION_BASE_DIR}/releases/current/vendor/bin/typo3cms database:export | gzip > ${PRODUCTION_BASE_DIR}/backups/${TIMESTAMP}.sql.gz
    fi

    # run
    ln -sfn ${PRODUCTION_BASE_DIR}/releases/${TIMESTAMP}/ releases/current

    # migrate
    $PHP_BIN releases/current/vendor/bin/typo3cms database:updateschema

    # post-run
    $PHP_BIN releases/current/vendor/bin/typo3cms cache:flush --force

    # cleanup
    cd ${PRODUCTION_BASE_DIR}/releases/
    for f in \$(ls | grep -v current | sort -r | tail -n +4); do
        echo "Deleting \$f"
        rm -rf \$f
    done
_EOF_