## Requirements

#### Your maschine

- Git Client

#### Server

- Linux with SSH-Server
- Rsync

## How it works

The GitLab runner builds the complete package of the TYPO3 website.
Next the package gets transported to a cache folder on the destination server. 
From there, the folders content is copied to a release folder that use the current timestamp
as its name. At last, a symlink named _current_ is created that points to the released folder.

## GitLab Configuration

Add the following variables for the deployment

Variable name | Description |
--- | --- 
PRODUCTION_SSH_USER | The SSH username for the destination server
PRODUCTION_SSH_HOST | The destination servers hostname
PRODUCTION_BASE_DIR | The absolute path on the destination server
PRODUCTION_SSH_KEY | The private SSH key for authenticating to the destination server
PRODUCTION_SSH_FINGERPRINT | The fingerprint for the given SSH user

## Destination server folder structure

Example:

_PRODUCTION_BASE_DIR_ is set to `/var/www/html`. This folder structure is created in that directory

-- releases<br />
---- 1534693768<br />
---- current (symlink)<br /> 
-- backups<br />
-- cache<br /> 
-- logs<br /> 
-- shared<br /> 
---- data<br /> 
------ fileadmin